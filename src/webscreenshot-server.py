# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
import argparse
import os
import sys

from aiohttp import web
from ddtrace import tracer, patch
from ddtrace.contrib.aiohttp import trace_app

# patch third-party modules like aiohttp_jinja2
patch(aiohttp=True)

from ekona_webscreenshot.server import WebScreenshotService

if __name__ == '__main__':
    logger = logging.getLogger(__name__)

    # Configure logging
    formatter = logging.Formatter('[%(asctime)s|%(threadName)s|%(levelname)s] %(message)s')

    handler = logging.StreamHandler(stream=sys.stdout)
    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)

    logger.propagate = False
    logger.handlers = []
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    # Parse arguments from commandline
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", help="hostname", action="store", dest="host", default='0.0.0.0')
    parser.add_argument("--port", help="port", action="store", dest="port", default=8000)
    args = parser.parse_args(sys.argv[1:])

    # ...or parse from env
    HOST = os.getenv('HOST', args.host)
    PORT = int(os.getenv('PORT', args.port))

    SECURE = os.getenv('SECURE', '1') in ('1', 'true')

    # Create WebScreenshotService
    if SECURE:
        shared_key = os.getenv('SHARED_KEY', '').encode('utf-8')
        if not shared_key:
            logger.error('Missing required environment setting "SHARED_KEY"')
            exit(1)
    else:
        shared_key = None

    logger.debug('starting webscreenservice (secure: {})'.format(SECURE))
    service = WebScreenshotService(shared_key=shared_key)

    # Define app
    app = service.get_application()

    # trace your application handlers
    trace_app(app, tracer, service='async-api')
    app['datadog_trace']['service'] = os.getenv('DD_SERVICE_NAME', 'ekona-webscreenshot')

    # Run application
    web.run_app(service.get_application(), host=HOST, port=PORT)
