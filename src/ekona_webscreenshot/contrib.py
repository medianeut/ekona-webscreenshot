# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import binascii
import base64
import json
import os
from collections import OrderedDict
from typing import Any, Dict, Optional
from urllib.parse import quote_plus, urlencode

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import cmac
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.ciphers import algorithms
from cryptography.hazmat.primitives.kdf.hkdf import HKDF

SIGNED_HEADER_SPLIT_CHAR = ':'


defaults_settings = {
    'viewport': {
        'width': 800,
        'height': 600,
        'deviceScaleFactor': 1.0,
        'isMobile': 0,
        'hasTouch': 0,
        'isLandscape': 0
    },
    'thumbnail': {
        'width': -1,
        'height': -1
    },
    'options': {
        'waitFor': False
    }
}



def hexlify_string(data):
    return binascii.hexlify(data).decode('utf-8')


def unhexlify_string(data):
    return binascii.unhexlify(data.encode('utf-8'))


def pack_request_data(url: str, **kwargs) -> bytes:
    return json.dumps(dict({
        'url': url,
    }, **kwargs), sort_keys=True).encode('utf-8')


class InvalidMAC(Exception):
    pass


def verify_cmac(shared_key: bytes, msg_cmac: str, url: str, viewport: Dict[str, Any], thumbnail: Dict[str, Any], options: Dict[str, Any]):
    if shared_key is None:
        raise InvalidMAC('shared_key is None.')

    if msg_cmac is None:
        raise InvalidMAC('msg_cmac is None.')

    # Unhexlify message_authentication_code
    msg_cmac = unhexlify_string(msg_cmac)
    salt = msg_cmac[:16]
    msg_cmac = msg_cmac[16:]

    # Recreate data
    data = pack_request_data(url=url, viewport=viewport, thumbnail=thumbnail, options=options)

    # Derive key
    info = b"ekona-webscreenshot-cmac-key"
    hkdf = HKDF(
        algorithm=hashes.SHA256(),
        length=16,
        salt=salt,
        info=info,
        backend=default_backend())
    symm_key = hkdf.derive(shared_key)

    # Verify cipher-based message authentication code (CMAC)
    c = cmac.CMAC(algorithms.AES(symm_key), backend=default_backend())
    c.update(data)
    try:
        c.verify(msg_cmac)
    except BaseException as e:
        raise InvalidMAC(e)


def create_cmac(shared_key: bytes, url: str, viewport: Dict[str, Any], thumbnail: Dict[str, Any], options: Dict[str, Any]):
    data = pack_request_data(url=url, viewport=viewport, thumbnail=thumbnail, options=options)

    # Derive key
    salt = os.urandom(16)
    info = b"ekona-webscreenshot-cmac-key"
    hkdf = HKDF(
        algorithm=hashes.SHA256(),
        length=16,
        salt=salt,
        info=info,
        backend=default_backend())
    symm_key = hkdf.derive(shared_key)

    # Create cipher-based message authentication code (CMAC)
    c = cmac.CMAC(algorithms.AES(symm_key), backend=default_backend())
    c.update(data)
    return hexlify_string(salt + c.finalize())


def build_viewport_settings(
        width: int=defaults_settings['viewport']['width'],
        height: int=defaults_settings['viewport']['height'],
        deviceScaleFactor: float=defaults_settings['viewport']['deviceScaleFactor'],
        isMobile: bool=defaults_settings['viewport']['isMobile'] == 1,
        hasTouch: bool=defaults_settings['viewport']['hasTouch'] == 1,
        isLandscape: bool=defaults_settings['viewport']['isLandscape'] == 1) -> Dict[str, Any]:
    return {
        'width': width,
        'height': height,
        'deviceScaleFactor': deviceScaleFactor,
        'isMobile': 1 if isMobile else 0,
        'hasTouch': 1 if hasTouch else 0,
        'isLandscape': 1 if isLandscape else 0
    }


def build_thumbnail_settings(
        width: int=defaults_settings['thumbnail']['width'],
        height: int=defaults_settings['thumbnail']['height']) -> Dict[str, Any]:
    return {
        'width': width,
        'height': height
    }

def build_options_settings(
        waitFor: int=defaults_settings['options']['waitFor']) -> Dict[str, Any]:
    return {
        'waitFor': waitFor
    }


def build_params(shared_key: bytes, url: str, viewport: Optional[Dict[str, Any]]=None,
                 thumbnail: Optional[Dict[str, Any]]=None, options: Optional[Dict[str, Any]]=None) -> OrderedDict:
    result = []

    # Add viewport settings
    viewport = viewport or build_viewport_settings()
    for key in viewport:
        if viewport[key] == defaults_settings['viewport'][key]:
            continue
        result.append(['viewport.{}'.format(key), viewport[key]])

    options = options or build_options_settings()
    for key in options:
        if options[key] == defaults_settings['options'][key]:
            continue
        result.append(['options.{}'.format(key), options[key]])

    # Add thumbnail settings
    thumbnail = thumbnail or build_thumbnail_settings()
    for key in thumbnail:
        if thumbnail[key] == defaults_settings['thumbnail'][key]:
            continue
        result.append(['thumbnail.{}'.format(key), thumbnail[key]])

    # Add cmac
    result.append(['cmac', create_cmac(shared_key=shared_key, url=url, viewport=viewport, thumbnail=thumbnail, options=options)])

    return OrderedDict(result)


def build_relative_url(shared_key: bytes, url: str, viewport: Optional[Dict[str, Any]]=None,
                       thumbnail: Optional[Dict[str, Any]]=None, options: Optional[Dict[str, Any]]=None, encode_base64: bool=False) -> str:
    params = build_params(shared_key=shared_key, url=url, viewport=viewport,  thumbnail=thumbnail, options=options)

    if encode_base64:
        params_json = json.dumps(dict(url=url, **params), sort_keys=True)
        encoded_params = base64.b64encode(params_json.encode('utf-8'))
        return '/b/{}'.format(encoded_params.decode('utf-8'))

    return '/{encoded_url}?{encoded_params}'.format(
        encoded_url=quote_plus(url), encoded_params=urlencode(params))
