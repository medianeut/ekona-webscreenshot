# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
import os
import tempfile

from PIL import Image
from pyppeteer import launch
from pyppeteer.errors import NetworkError

logger = logging.getLogger(__name__)


class ConnectionError(Exception):
    pass


class InvalidResponseStatus(Exception):
    def __init__(self, status_code, *args):
        super(Exception, self).__init__(*args)
        self.status_code = status_code


async def make_screenshot(url, viewport, thumbnail, options):
    browser = await launch(
        headless=True,
        executablePath="/usr/bin/chromium-browser",
        args=['--no-sandbox', '--disable-gpu', '--disable-dev-shm-usage'])

    logger.debug('fetching screenshot from {} with viewport {}...'.format(url, viewport))

    page = await browser.newPage()
    await page.setViewport(viewport)

    try:
        response = await page.goto(url)
        if response.status != 200:
            raise InvalidResponseStatus(response.status, str(response))
    except NetworkError as e:
        raise ConnectionError(e)

    if options and options['waitFor']:
        await page.waitFor(options['waitFor'])

    fhandle, fname = tempfile.mkstemp(prefix='screenshot', suffix='.png')
    await page.screenshot({'path': fname})
    os.close(fhandle)

    await browser.close()

    thumbnail_dimensions = thumbnail.get('width', -1), thumbnail.get('height', -1)
    if thumbnail and all([thumbnail_dimensions[0] != -1, thumbnail_dimensions[1] != -1]):
        logger.debug('Creating thumbnail ({})'.format(thumbnail_dimensions))
        im = Image.open(fname)
        im.thumbnail(thumbnail_dimensions)
        im.save(fname, "PNG")

    return fname
