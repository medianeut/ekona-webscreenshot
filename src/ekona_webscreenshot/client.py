# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from typing import Optional, Dict, Any

from .contrib import build_relative_url


class WebScreenshotClientHelper(object):
    def __init__(self, host: str, port: int, shared_key: bytes, protocol='http'):
        self.host = host
        self.port = port
        self.shared_key = shared_key
        self.protocol = protocol

    def build_url(self, url: str, viewport: Optional[Dict[str, Any]]=None, thumbnail: Optional[Dict[str, Any]]=None,
                  options: Optional[Dict[str, Any]]=None, encode_base64: bool=False):
        relative_url = build_relative_url(
            shared_key=self.shared_key, url=url, viewport=viewport, thumbnail=thumbnail, options=options,
            encode_base64=encode_base64)

        if all([self.port == 80, self.protocol == 'http']) or all([self.port == 443, self.protocol == 'https']):
            return '{protocol}://{host}{path}'.format(protocol=self.protocol, host=self.host, path=relative_url)

        return '{protocol}://{host}:{port}{path}'.format(
            protocol=self.protocol, host=self.host, port=self.port, path=relative_url)
