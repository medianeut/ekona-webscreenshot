# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import io
import os
import tempfile
from collections import OrderedDict
from urllib.parse import quote_plus, urlencode

from PIL import Image
from PIL.PngImagePlugin import PngImageFile
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from ekona_webscreenshot.contrib import build_relative_url, build_viewport_settings, build_thumbnail_settings, \
    build_params
from ekona_webscreenshot.server import WebScreenshotService
from ekona_webscreenshot.tests import AsyncMock


def _get_file_path(path):
    file_path = os.path.abspath(os.path.join('ekona_webscreenshot', 'tests', path))
    assert os.path.isfile(file_path)
    return file_path


def _mock_web_screenshot():
    # Load mock screenshot data
    mock_screenshot_file = _get_file_path('./raw/test_screenshot.jpg')

    # Make tempfile
    fhandle, fname = tempfile.mkstemp(prefix='screenshot', suffix='.png')
    os.close(fhandle)

    # Convert (a copy) to PNG
    im = Image.open(mock_screenshot_file)
    im.save(fname, "PNG")

    return fname


class WebScreenshotServiceTests(AioHTTPTestCase):
    default_shared_key = b's3cr3t'

    async def get_application(self):
        service = WebScreenshotService(shared_key=self.default_shared_key)
        app = service.get_application()
        app.service = service
        return app

    @unittest_run_loop
    async def test_root(self):
        resp = await self.client.request('GET', '/')
        assert resp.status == 404

    @unittest_run_loop
    async def test_screenshot_no_cmac(self):
        page_url = '/{}'.format(quote_plus('https://www.facebook.com'))

        resp = await self.client.request('GET', page_url)
        assert resp.status == 401
        text = await resp.text()
        assert "Unauthorized" in text

    @unittest_run_loop
    async def test_screenshot(self):
        # Mock screenshot
        self.app.service._make_screenshot = AsyncMock(return_value=_mock_web_screenshot())

        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Build valid url
        url = build_relative_url(shared_key=self.default_shared_key, url=page_url)

        # Make request
        resp = await self.client.request('GET', url)
        assert resp.status == 200

        # Read bytes from stream
        image_data = await resp.read()
        assert image_data

        # Test bytes are a valid image
        image = Image.open(io.BytesIO(image_data))
        assert isinstance(image, PngImageFile)
        assert image.width == 1920  # Default for the raw mock image
        assert image.height == 1080  # Default for the raw mock image

    @unittest_run_loop
    async def test_screenshot_encode_base64(self):
        # Mock screenshot
        self.app.service._make_screenshot = AsyncMock(return_value=_mock_web_screenshot())

        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Build valid url
        url = build_relative_url(shared_key=self.default_shared_key, url=page_url, encode_base64=True)

        # Make request
        resp = await self.client.request('GET', url)
        assert resp.status == 200

        # Read bytes from stream
        image_data = await resp.read()
        assert image_data

        # Test bytes are a valid image
        image = Image.open(io.BytesIO(image_data))
        assert isinstance(image, PngImageFile)
        assert image.width == 1920  # Default for the raw mock image
        assert image.height == 1080  # Default for the raw mock image

    @unittest_run_loop
    async def test_screenshot_wrong_shared_key(self):
        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Build valid url
        url = build_relative_url(shared_key=b'blabla', url=page_url)

        # Make request
        resp = await self.client.request('GET', url)
        assert resp.status == 401
        text = await resp.text()
        assert "Unauthorized" in text

    @unittest_run_loop
    async def test_screenshot_tamper_url(self):
        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Configure the viewport settings
        viewport = build_viewport_settings(width=1920, height=1080)

        # Configure the thumbnail settings
        thumbnail = build_thumbnail_settings(width=400, height=300)

        # Build params
        params = build_params(shared_key=self.default_shared_key, url=page_url, viewport=viewport, thumbnail=thumbnail)
        assert isinstance(params, OrderedDict)

        # Tamper url
        page_url = 'www.twitter.com'

        # Build url
        url = '/{encoded_url}?{encoded_params}'.format(
            encoded_url=quote_plus(page_url), encoded_params=urlencode(params))

        # Make request
        resp = await self.client.request('GET', url)
        assert resp.status == 401
        text = await resp.text()
        assert "Unauthorized" in text

    @unittest_run_loop
    async def test_screenshot_tamper_viewport(self):
        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Configure the viewport settings
        viewport = build_viewport_settings(width=1920, height=1080)

        # Configure the thumbnail settings
        thumbnail = build_thumbnail_settings(width=400, height=300)

        # Build params
        params = build_params(shared_key=self.default_shared_key, url=page_url, viewport=viewport, thumbnail=thumbnail)
        assert isinstance(params, OrderedDict)

        # Tamper viewport
        params.update({
            'viewport.height': 720
        })

        # Build url
        url = '/{encoded_url}?{encoded_params}'.format(
            encoded_url=quote_plus(page_url), encoded_params=urlencode(params))

        # Make request
        resp = await self.client.request('GET', url)
        assert resp.status == 401
        text = await resp.text()
        assert "Unauthorized" in text

    @unittest_run_loop
    async def test_screenshot_tamper_thumbnail(self):
        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Configure the viewport settings
        viewport = build_viewport_settings(width=1920, height=1080)

        # Configure the thumbnail settings
        thumbnail = build_thumbnail_settings(width=400, height=300)

        # Build params
        params = build_params(shared_key=self.default_shared_key, url=page_url, viewport=viewport, thumbnail=thumbnail)
        assert isinstance(params, OrderedDict)

        # Tamper viewport
        params.update({
            'thumbnail.height': 720
        })

        # Build url
        url = '/{encoded_url}?{encoded_params}'.format(
            encoded_url=quote_plus(page_url), encoded_params=urlencode(params))

        # Make request
        resp = await self.client.request('GET', url)
        assert resp.status == 401
        text = await resp.text()
        assert "Unauthorized" in text
