# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import base64
from collections import OrderedDict
from unittest import TestCase, mock
from urllib.parse import urlparse, quote_plus, parse_qs

from ekona_webscreenshot.client import WebScreenshotClientHelper
from ekona_webscreenshot.contrib import build_viewport_settings, build_thumbnail_settings, build_params, \
    verify_cmac, InvalidMAC, create_cmac


class WebScreenshotTests(TestCase):
    def setUp(self):
        self.service_host = 'testserver'
        self.service_port = 80
        self.shared_key=b's3cr3t'

    def test_webscreenshot_build_url(self):
        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Configure the viewport settings
        viewport = build_viewport_settings(width=1920, height=1080)

        # Configure the thumbnail settings
        thumbnail = build_thumbnail_settings(width=400, height=300)

        # Build url
        url = WebScreenshotClientHelper(
            host=self.service_host, port=self.service_port, shared_key=self.shared_key).build_url(
                url=page_url, viewport=viewport, thumbnail=thumbnail)
        assert isinstance(url, str)

        # Parse result
        p = urlparse(url)
        assert p.scheme == 'http'
        assert p.hostname == self.service_host
        assert p.port is None  # Default http port (80)
        assert p.path == '/{}'.format(quote_plus(page_url))

        # Verify query params
        query_dict = parse_qs(p.query)
        assert set(query_dict.keys()) == {
            'viewport.width',
            'viewport.height',
            'thumbnail.width',
            'thumbnail.height',
            'cmac'
        }
        assert int(query_dict['viewport.width'][0]) == viewport['width']
        assert int(query_dict['viewport.height'][0]) == viewport['height']
        assert int(query_dict['thumbnail.width'][0]) == thumbnail['width']
        assert int(query_dict['thumbnail.height'][0]) == thumbnail['height']

        # Verify cmac
        verify_cmac(
            shared_key=self.shared_key, msg_cmac=query_dict['cmac'][0], url=page_url, viewport=viewport,
            thumbnail=thumbnail)

    def test_webscreenshot_build_url_encode_base64(self):
        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Configure the viewport settings
        viewport = build_viewport_settings(width=1920, height=1080)

        # Configure the thumbnail settings
        thumbnail = build_thumbnail_settings(width=400, height=300)

        # Precalculate cmac
        precalculated_cmac = create_cmac(
            shared_key=self.shared_key, url=page_url, viewport=viewport, thumbnail=thumbnail)

        # Build url
        with mock.patch('ekona_webscreenshot.contrib.create_cmac') as mock_create_cmac:
            mock_create_cmac.return_value = precalculated_cmac
            url = WebScreenshotClientHelper(
                host=self.service_host, port=self.service_port, shared_key=self.shared_key).build_url(
                    url=page_url, viewport=viewport, thumbnail=thumbnail, encode_base64=True)
            assert isinstance(url, str)

        # Construct expected path
        expected_data = json.dumps({
            'url': page_url,
            'viewport.width': viewport['width'],
            'viewport.height': viewport['height'],
            'thumbnail.width': thumbnail['width'],
            'thumbnail.height': thumbnail['height'],
            'cmac': precalculated_cmac
        }, sort_keys=True).encode('utf-8')
        expected_path = '/b/{}'.format(base64.b64encode(expected_data).decode('utf-8'))

        # Parse result
        p = urlparse(url)
        assert p.scheme == 'http'
        assert p.hostname == self.service_host
        assert p.port is None  # Default http port (80)
        assert p.path == expected_path

    def test_webscreenshot_message_authentication_minimal(self):
        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Build params
        params = build_params(shared_key=self.shared_key, url=page_url)
        assert isinstance(params, OrderedDict)

        # Verify cmac
        verify_cmac(shared_key=self.shared_key, msg_cmac=params['cmac'], url=page_url,
                    viewport=build_viewport_settings(), thumbnail=build_thumbnail_settings())

    def test_webscreenshot_message_authentication(self):
        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Configure the viewport settings
        viewport = build_viewport_settings(width=1920, height=1080)

        # Configure the thumbnail settings
        thumbnail = build_thumbnail_settings(width=400, height=300)

        # Build params
        params = build_params(shared_key=self.shared_key, url=page_url, viewport=viewport, thumbnail=thumbnail)
        assert isinstance(params, OrderedDict)

        # Verify cmac
        verify_cmac(
            shared_key=self.shared_key, msg_cmac=params['cmac'], url=page_url, viewport=viewport, thumbnail=thumbnail)

    def test_webscreenshot_message_authentication_tamper_url(self):
        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Configure the viewport settings
        viewport = build_viewport_settings(width=1920, height=1080)

        # Configure the thumbnail settings
        thumbnail = build_thumbnail_settings(width=400, height=300)

        # Build params
        params = build_params(shared_key=self.shared_key, url=page_url, viewport=viewport, thumbnail=thumbnail)
        assert isinstance(params, OrderedDict)

        # Tamper url
        page_url = 'www.twitter.com'

        # Verify cmac
        with self.assertRaises(InvalidMAC):
            verify_cmac(
                shared_key=self.shared_key, msg_cmac=params['cmac'], url=page_url, viewport=viewport,
                thumbnail=thumbnail)

    def test_webscreenshot_message_authentication_tamper_viewport(self):
        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Configure the viewport settings
        viewport = build_viewport_settings(width=1920, height=1080)

        # Configure the thumbnail settings
        thumbnail = build_thumbnail_settings(width=400, height=300)

        # Build params
        params = build_params(shared_key=self.shared_key, url=page_url, viewport=viewport, thumbnail=thumbnail)
        assert isinstance(params, OrderedDict)

        # Tamper viewport
        viewport.update({
            'height': 720
        })

        # Verify cmac
        with self.assertRaises(InvalidMAC):
            verify_cmac(
                shared_key=self.shared_key, msg_cmac=params['cmac'], url=page_url, viewport=viewport,
                thumbnail=thumbnail)

    def test_webscreenshot_message_authentication_tamper_thumbnail(self):
        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Configure the viewport settings
        viewport = build_viewport_settings(width=1920, height=1080)

        # Configure the thumbnail settings
        thumbnail = build_thumbnail_settings(width=400, height=300)

        # Build params
        params = build_params(shared_key=self.shared_key, url=page_url, viewport=viewport, thumbnail=thumbnail)
        assert isinstance(params, OrderedDict)

        # Tamper thumbnail
        thumbnail.update({
            'height': 720
        })

        # Verify cmac
        with self.assertRaises(InvalidMAC):
            verify_cmac(
                shared_key=self.shared_key, msg_cmac=params['cmac'], url=page_url, viewport=viewport,
                thumbnail=thumbnail)

    def test_webscreenshot_message_authentication_wrong_shared_key(self):
        # Set the page for which you would like to grab the screenshot
        page_url = 'https://www.facebook.com'

        # Configure the viewport settings
        viewport = build_viewport_settings(width=1920, height=1080)

        # Configure the thumbnail settings
        thumbnail = build_thumbnail_settings(width=400, height=300)

        # Build params
        params = build_params(shared_key=self.shared_key, url=page_url, viewport=viewport, thumbnail=thumbnail)
        assert isinstance(params, OrderedDict)

        # Tamper shared_key
        shared_key = b'blabla'

        # Verify cmac
        with self.assertRaises(InvalidMAC):
            verify_cmac(
                shared_key=shared_key, msg_cmac=params['cmac'], url=page_url, viewport=viewport,
                thumbnail=thumbnail)
