# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import json
import base64
import logging

from aiohttp import web

from ekona_webscreenshot.contrib import InvalidMAC
from .contrib import verify_cmac, defaults_settings
from .logic import make_screenshot, InvalidResponseStatus, ConnectionError

logger = logging.getLogger(__name__)


class WebScreenshotService(object):
    def __init__(self, shared_key: bytes=None):
        self.shared_key = shared_key
        self._make_screenshot = make_screenshot

    def get_application(self):
        # Define application
        app = web.Application()
        app.router.add_get(r'/b/{b64q:(.+)}', self.handle)
        app.router.add_get('/{url}', self.handle)
        return app

    async def handle(self, request):
        if 'b64q' in request.match_info:
            params_json = base64.b64decode(request.match_info['b64q'].encode('utf-8'))
            params = json.loads(params_json)
        else:
            params = dict({
                'url': request.match_info.get('url', None)
            }, **request.query)

        if 'url' not in params or not params['url']:
            return web.Response(text='Not Found. Params: {}'.format(
                json.dumps(params, sort_keys=True)), status=404)

        url = params['url']

        viewport = {
            'width': int(params.get('viewport.width', defaults_settings['viewport']['width'])),
            'height': int(params.get('viewport.height', defaults_settings['viewport']['height'])),
            'deviceScaleFactor': float(
                params.get('viewport.deviceScaleFactor', defaults_settings['viewport']['deviceScaleFactor'])),
            'isMobile': int(params.get('viewport.isMobile', defaults_settings['viewport']['isMobile'])),
            'hasTouch': int(params.get('viewport.hasTouch', defaults_settings['viewport']['hasTouch'])),
            'isLandscape': int(params.get('viewport.isLandscape', defaults_settings['viewport']['isLandscape']))
        }

        thumbnail = {
            'width': int(params.get('thumbnail.width', defaults_settings['thumbnail']['width'])),
            'height': int(params.get('thumbnail.height', defaults_settings['thumbnail']['height']))
        }

        options = {
            'waitFor': params.get('options.waitFor', defaults_settings['options']['waitFor'])
        }

        if self.shared_key is not None:
            msg_cmac = params.get('cmac', None)

            if not msg_cmac:
                return web.Response(text='Unauthorized', status=401)

            try:
                verify_cmac(shared_key=self.shared_key, msg_cmac=msg_cmac, url=url, viewport=viewport, thumbnail=thumbnail, options=options)
            except InvalidMAC:
                return web.Response(text='Unauthorized', status=401)
            except BaseException as e:
                return web.Response(text='Error: {}.'.format(e), status=500)

        # Update viewport boolean's to match protocol
        for key in ('isMobile', 'hasTouch', 'isLandscape'):
            viewport[key] = viewport[key] == 1

        # Grab screenshot
        result = await self.grab_screenshot(url=url, viewport=viewport, thumbnail=thumbnail, options=options)

        return result

    async def grab_screenshot(self, url: str, viewport: dict, thumbnail: dict, options: dict):
        logger.debug('grab_screenshot: url: {}, viewport: {}, thumbnail: {}'.format(url, viewport, thumbnail))
        try:
            result = await self._make_screenshot(url=url, viewport=viewport, thumbnail=thumbnail, options=options)
        except ConnectionError as e:
            logger.exception('Not Found: {}. Error: {}'.format(url, e))
            return web.Response(text='Not Found: {}'.format(url), status=404)
        except InvalidResponseStatus as e:
            logger.exception('Unexpected Status: {}: {}'.format(e.status_code, e))
            return web.Response(text='Unexpected Status: {}: {}'.format(e.status_code, e), status=e.status_code)
        except BaseException as e:
            logger.exception('Error while makeing screenshot: {}'.format(e))
            return web.Response(text='Error while makeing screenshot: {}'.format(e), status=500)

        fh = None
        try:
            # Open image file
            fh = open(result, 'rb')

            # Return image in response
            return web.Response(body=fh.read(), content_type='image/png')
        finally:
            # Close filehandle
            if fh is not None:
                fh.close()

            # Remove temporary file
            if os.path.exists(result):
                logger.debug('deleting tempfile: {}'.format(result))
                os.remove(result)
