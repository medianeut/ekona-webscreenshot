#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def get_version(*file_paths):
    """Retrieves the version from ekona_client/__init__.py"""
    filename = os.path.join(os.path.dirname(__file__), *file_paths)
    version_file = open(filename).read()
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError('Unable to find version string.')


version = get_version("src", "ekona_webscreenshot", "__init__.py")


if sys.argv[-1] == 'publish':
    try:
        import wheel
        print("Wheel version: ", wheel.__version__)
    except ImportError:
        print('Wheel library missing. Please run "pip install wheel"')
        sys.exit()
    os.system('python setup.py sdist upload')
    os.system('python setup.py bdist_wheel upload')
    sys.exit()

if sys.argv[-1] == 'tag':
    print("Tagging the version on git:")
    os.system("git tag -a %s -m 'version %s'" % (version, version))
    os.system("git push --tags")
    sys.exit()

setup(
    name='ekona-webscreenshot',
    version=version,
    description="""A tool to make screenshots of live websites""",
    author='Dirk Moors',
    author_email='technology@nurama.com',
    url='https://bitbucket.org/medianeut/ekona-webscreenshot',
    package_dir = {'': 'src'},
    packages=[
        'ekona_webscreenshot',
    ],
    include_package_data=True,
    install_requires=[
        'cryptography>=2.3,<3.0',
        'pytz>=2018.5',
    ],
    extras_require = {
        'server':  [
            "aiohttp>=3.4.4,<4.0.0",
            "pyppeteer==0.0.25",
            "ddtrace>=0.16.0,<1.0.0"
        ]
    },
    license="MIT",
    zip_safe=False,
    keywords='ekona-webscreenshot',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
)
