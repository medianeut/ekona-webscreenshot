# Ekona WebScreenshot 


### Server
To run the server, make sure you set the following environment variable:
```
SHARED_KEY=mys3cr3t
```
An example docker-compose file could look like:
```yaml
version: '2'
services:
  webscreenshot:
    build: .
    environment:
      - SHARED_KEY=mys3cr3t
    ports:
      - "8000:8000"
```


### Client
Then, when you want to fetch the screenshot for the Facebook homepage, you need to add a signature to your request:
```python
from ekona_webscreenshot.client import WebScreenshotClientHelper
from ekona_webscreenshot.contrib import build_viewport_settings

helper = WebScreenshotClientHelper(host='localhost', port=8080, shared_key=b'mys3cr3t')

# Set the page for which you would like to grab the screenshot
page_url = 'https://www.facebook.com'

# E.g. prepare a custom viewport
viewport = build_viewport_settings(width=1920, height=1080)

# Build the signed url
signed_url = helper.build_url(url=page_url, viewport=viewport)

# Now use the signed url to request the screenshot. Easy peasy!
```
