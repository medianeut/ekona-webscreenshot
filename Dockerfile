FROM python:3.6.8-alpine3.10

ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    LC_CTYPE="en_US.UTF-8" \
    LC_ALL=en_US.UTF-8 \
    PYTHONUNBUFFERED=1

# Install ca-certificates
RUN set -ex \
    && apk add --update make ca-certificates curl linux-headers \
    && update-ca-certificates \
    && rm -rf /var/cache/apk/*

# Install common packages
RUN set -ex \
    && PKGS='bash zlib su-exec linux-headers make curl wget tar' \
    && apk add --update ${PKGS} \
    && rm -rf /var/cache/apk/*

ENV PYTHONUNBUFFERED=1 \
    ROOT="/data"

ENV SRC_DIR="${ROOT}/src" \
    DEPLOYMENT_DIR="${ROOT}/deployment"

ENV CHOWNDIRS="${SRC_DIR}"

COPY docker-entrypoint.sh /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

# Install Python Cryptography (statically linked with OpenSSL 1.1.0h)
ENV CRYPTOGRAPHY_VERSION=2.3 \
    OPENSSL_VERSION=1.1.0h \
    OPENSSL_GPG_KEY=8657ABB260F056B1E5190839D9C4D26D0E604491
RUN set -v \
    && BUILD_PKGS='gcc g++ perl libffi-dev gnupg' \
    && apk add --update ${BUILD_PKGS} \
    && wget -q https://raw.githubusercontent.com/dirkmoors/docker-alpine-recipes/master/python-cryptography/install-cryptography.sh \
    && sh install-cryptography.sh \
    && rm -rf install-cryptography.sh \
    && apk del --purge ${BUILD_PKGS}

# Install Pillow Dependencies (currently, these are the only versions available. Keep them here for explicit version pinning.)
ENV IMAGEQUANT_VERSION=2.8.2 \
    WEBP_VERSION=0.6.0
RUN set -ex \
    && BUILD_PKGS='git gcc g++' \
    && PILLOW_DEPS='zlib jpeg-dev zlib-dev tiff-dev lcms2-dev freetype-dev openjpeg-dev tk-dev tcl-dev' \
    && apk add --update ${PILLOW_DEPS} ${BUILD_PKGS} \
    && wget -q https://raw.githubusercontent.com/dirkmoors/docker-alpine-recipes/master/libimagequant/install-libimagequant.sh \
    && sh install-libimagequant.sh \
    && wget -q https://raw.githubusercontent.com/dirkmoors/docker-alpine-recipes/master/libwebp/install-libwebp.sh \
    && sh install-libwebp.sh \
    && apk del --purge ${BUILD_PKGS} \
    && find / -name \*.pyc -delete \
    && rm -rf /var/cache/apk/*

COPY deployment/requirements.txt ${DEPLOYMENT_DIR}/requirements.txt

WORKDIR ${SRC_DIR}

# Install packages
RUN set -v \
    && BUILD_PKGS='git gcc g++ python-dev libffi-dev' \
    && apk add --no-cache ${BUILD_PKGS} \
    && pip install -U pip \
    && pip install -r ${DEPLOYMENT_DIR}/requirements.txt \
    && apk del --purge ${BUILD_PKGS}

RUN apk add --no-cache chromium udev ttf-freefont harfbuzz nss
RUN chromium-browser --product-version

# Installs Chromium (72.0.3626.109-r0) package.
#RUN apk update && apk upgrade && \
#    echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
#    echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
#    apk add --no-cache \
#      chromium@edge=73.0.3683.103-r0 \
#      harfbuzz@edge=2.5.2-r0 \
#      nss@edge=3.44-r0

# Copy source
COPY src ${SRC_DIR}

# Set default command
CMD ["python", "webscreenshot-server.py"]
